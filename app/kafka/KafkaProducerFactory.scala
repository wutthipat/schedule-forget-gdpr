package com.scale360solutions.gdprloggerhashing.kafka;
import java.util.Properties

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.common.serialization.StringSerializer

object KafkaProducerFactory {
  def init(): Properties = {
    val properties = new Properties()
    properties.put("bootstrap.servers", "172.18.0.1:9092")
    properties.put("key.serializer", classOf[StringSerializer])
    properties.put("value.serializer", classOf[StringSerializer])
    properties
  }

  def create(properties: Properties = init): KafkaProducer[String, String] = {
    val kafkaProducer = new KafkaProducer[String, String](properties)

    kafkaProducer
  }
}
