package com.scale360solutions.gdprloggerhashing.kafka

import java.util.Properties
import java.util.concurrent.TimeUnit

import com.scale360solutions.gdprloggerhashing.model.OwnerId
import org.apache.kafka.clients.producer.{ProducerRecord, RecordMetadata}

import scala.concurrent.Future

class KafkaProducerService() {
  val forgetTopic = "forget-test"
  val kafkaProducer = KafkaProducerFactory.create()
  def sendForgetMessage(ownerId: OwnerId): Future[RecordMetadata] = {
    val record = new ProducerRecord[String, String](forgetTopic, ownerId.id, ownerId.id)

    System.out.println(record + "have been sent.")
    Future.successful{kafkaProducer.send(record).get(3000, TimeUnit.MILLISECONDS)}
  }
}
