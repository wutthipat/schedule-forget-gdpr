package com.scale360solutions.gdprloggerhashing.repositories

import com.scale360solutions.gdprloggerhashing.model.{ForgetUserJob, JobId}
import org.joda.time.DateTime

import scala.concurrent.Future
;

trait ForgetUserJobRepository {
  def insert(forgetUserJob: ForgetUserJob): Future[JobId]
  def findByCurrentDateTimeGtOrEqTargetDateTimeAndRemovedFalse(currentDateTime: DateTime): Future[Seq[ForgetUserJob]]
  def updateRemoveFlagTrueByJobList(forgetUserJobList: List[ForgetUserJob])
}
