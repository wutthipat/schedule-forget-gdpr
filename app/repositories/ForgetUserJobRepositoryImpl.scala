package com.scale360solutions.gdprloggerhashing.repositories
import java.sql.Timestamp

import com.scale360solutions.gdprloggerhashing.model.{TargetDateTime, _}
import com.scale360solutions.gdprloggerhashing.repositories.connection.{DBComponent, MySqlDBComponent}
import org.joda.time.DateTime

trait ForgetUserJobTable { this: DBComponent =>

  import driver.api._

  implicit val JobIdColumnType = MappedColumnType.base[JobId, Long](
    s => s.id,
    long => JobId(long)
  )

  implicit val OwnerIdColumnType = MappedColumnType.base[OwnerId, String](
    s => s.id,
    str => OwnerId(str)
  )

  implicit val TargetDateTimeColumnType = MappedColumnType.base[TargetDateTime, Timestamp] (
    targetDateTime => new Timestamp(targetDateTime.value.getMillis),
    timestamp => TargetDateTime(new DateTime(timestamp))
  )

  implicit val RemoveFlagColumnType = MappedColumnType.base[RemoveFlag, Boolean] (
    removeFlag => removeFlag.value,
    boolean => RemoveFlag(boolean)
  )

  class ForgetUserJobTable(tag: Tag) extends Table[ForgetUserJob](tag, "FORGET_USER_JOB"){
    // Columns
    def jobId = column[JobId]("JOB_ID", O.AutoInc, O.PrimaryKey, O.Unique)
    def ownerId = column[OwnerId]("OWNER_ID", O.Length(128))
    def targetDateTime = column[TargetDateTime]("TARGET_DATE_TIME", O.Default(TargetDateTime(new DateTime(System.currentTimeMillis()))))
    def removeFlag = column[RemoveFlag]("REMOVED", O.Default(RemoveFlag(false)))

    override def * = (jobId, ownerId, targetDateTime, removeFlag) <> (ForgetUserJob.tupled, ForgetUserJob.unapply)
  }

  val forgetUserJobs = TableQuery[ForgetUserJobTable]
}

trait ForgetUserJobSqlRepository extends ForgetUserJobTable with ForgetUserJobRepository{ this: DBComponent =>
  import driver.api._
  override def insert(forgetUserJob: ForgetUserJob) = {
    db.run((forgetUserJobs returning forgetUserJobs.map(_.jobId)) += forgetUserJob)
  }

  override def findByCurrentDateTimeGtOrEqTargetDateTimeAndRemovedFalse(currentDateTime: DateTime) = db.run {
      forgetUserJobs.filter(x => x.targetDateTime <= TargetDateTime(new DateTime(currentDateTime.getMillis)) && x.removeFlag === RemoveFlag(false)).result
  }

  override def updateRemoveFlagTrueByJobList(forgetUserJobList: List[ForgetUserJob]) = db.run{
    DBIO.sequence(forgetUserJobList.map(forgetUserJob => {
      forgetUserJobs.filter(_.jobId === forgetUserJob.jobId).map(job => job.removeFlag).update(RemoveFlag(true))
    }))
  }

}

class ForgetUserJobRepositoryImpl extends MySqlDBComponent with ForgetUserJobSqlRepository
