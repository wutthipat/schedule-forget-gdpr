package dto

import play.api.libs.json.{Json, Writes}

case class ForgetUserResponseDto(ownerId: Option[String] = None, jobId: Option[Long] = None)
object ForgetUserResponseDto{
  implicit val forgetUserResponseDtoWrites: Writes[ForgetUserResponseDto] = Json.writes[ForgetUserResponseDto]
}
