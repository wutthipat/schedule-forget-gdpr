package com.scale360solutions.dto

import play.api.libs.json.{Json, Reads, Writes}


case class ForgetUserDto(ownerId: String, timeToForgetInMinute: Long)

object ForgetUserDto{
  implicit val forgetUserDtoReads: Reads[ForgetUserDto] = Json.reads[ForgetUserDto]
  implicit val forgetUserDtoWrites: Writes[ForgetUserDto] = Json.writes[ForgetUserDto]
}





