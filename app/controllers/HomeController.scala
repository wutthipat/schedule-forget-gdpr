package controllers

import javax.inject._

import com.scale360solutions.gdprloggerhashing.model.OwnerId
import com.scale360solutions.gdprloggerhashing.services.ForgetUserScheduleService
import play.api.libs.json.{JsString, Json}
import play.api.mvc._
import com.scale360solutions.dto._

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}
import dto.ForgetUserResponseDto

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(cc: ControllerComponents, forgetUserScheduleService: ForgetUserScheduleService) extends AbstractController(cc) {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    println("Index")
    Ok(views.html.index())
  }

  def forgetUserWithScheduler() = Action { request =>
    println("forgetUserWithScheduler")
    val requestBody = request.body.asJson
    val forgetUserDto = requestBody.map(x => x.validate[ForgetUserDto])
    if(forgetUserDto.isDefined) {
      forgetUserDto.get.fold(
        error => BadRequest(Json.obj("message" -> JsString.apply("The type of parameter is invalid"))),
        forgetUserDto => {
          val respFuture = forgetUserScheduleService.forgetUser(OwnerId(forgetUserDto.ownerId), forgetUserDto.timeToForgetInMinute)
          val respTry = Await.ready(respFuture, Duration.Inf).value.get
          respTry match {
            case Success(resp) =>{
              println("Success")
              resp match{
                case ForgetUserResponseDto(Some(_), None) => Ok(Json.toJson(resp))
                case ForgetUserResponseDto(None, Some(_)) => Created(Json.toJson(resp))
              }
            }
            case Failure(e) => {println("Error occured"); throw e}
          }
        }
      )
    }else BadRequest(Json.obj("message" -> JsString.apply("The format of json  is invalid")))
  }
}
