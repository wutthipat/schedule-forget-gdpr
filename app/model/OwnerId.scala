package com.scale360solutions.gdprloggerhashing.model

import play.api.libs.json.{Json, Writes}

case class OwnerId(id: String)
object OwnerId{
  implicit val ownerIdWrites: Writes[OwnerId] = Json.writes[OwnerId]
}
