package com.scale360solutions.gdprloggerhashing.model

import org.joda.time.DateTime
import play.api.libs.json.{Json, Writes}

case class ForgetUserJob(jobId: JobId = null, ownerId: OwnerId, targetDateTime: TargetDateTime, removed: RemoveFlag = RemoveFlag(false))

case class JobId(id: Long)
object JobId{
  implicit val jobIdWrites: Writes[JobId] = Json.writes[JobId]
}

case class TargetDateTime(value: DateTime)

case class RemoveFlag(value: Boolean)
