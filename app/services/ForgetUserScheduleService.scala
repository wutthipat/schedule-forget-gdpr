package com.scale360solutions.gdprloggerhashing.services

import javax.inject.Inject

import com.scale360solutions.gdprloggerhashing.kafka.KafkaProducerService
import com.scale360solutions.gdprloggerhashing.model.{ForgetUserJob, JobId, OwnerId, TargetDateTime}
import com.scale360solutions.gdprloggerhashing.repositories.ForgetUserJobRepositoryImpl
import dto.ForgetUserResponseDto
import org.joda.time.DateTime

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class ForgetUserScheduleService  @Inject()(kafkaProducerService: KafkaProducerService, forgetUserJobSqlRepositoryImpl: ForgetUserJobRepositoryImpl) {

  def forgetUser(ownerId: OwnerId, delayInMinutes: Long): Future[ForgetUserResponseDto] = delayInMinutes match {
    case x if x != 0 => {
      //      system.scheduler.schedule(schedulerPeriodicDelay, schedulerPeriodicDelay)(removeExpired)
      val delayInMilliSecs = delayInMinutes * 60 * 1000
      val now = System.currentTimeMillis()
      val dateTime = new DateTime(now + delayInMilliSecs)
      println(ownerId + " will be deleted in " + dateTime)
      forgetUserJobSqlRepositoryImpl.insert(ForgetUserJob(ownerId = ownerId, targetDateTime = TargetDateTime(dateTime))).map(jobId => jobIdToForgetUserResponseDto(jobId.id))
    }
    case _ => publishForgetMsgToKafka(ownerId).map(_ => recordMetadataToForgetUserResponseDto(ownerId.id))
  }

  def publishForgetMsgToKafka(ownerId: OwnerId) = {
    kafkaProducerService.sendForgetMessage(ownerId)
  }

  def jobIdToForgetUserResponseDto(jobId: Long): ForgetUserResponseDto = ForgetUserResponseDto(jobId = Some(jobId))
  def recordMetadataToForgetUserResponseDto(ownerId: String): ForgetUserResponseDto = ForgetUserResponseDto(ownerId = Some(ownerId))
}

