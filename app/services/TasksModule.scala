package com.scale360solutions.gdprloggerhashing.services

import play.api.inject.{SimpleModule, _}

class TasksModule extends SimpleModule(bind[ForgetJobScheduler].toSelf.eagerly())
