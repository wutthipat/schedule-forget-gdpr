package com.scale360solutions.gdprloggerhashing.services

import javax.inject.Inject

import akka.actor.ActorSystem
import com.scale360solutions.gdprloggerhashing.kafka.KafkaProducerService
import com.scale360solutions.gdprloggerhashing.repositories.ForgetUserJobRepositoryImpl
import org.joda.time.DateTime

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.{Failure, Success}

class ForgetJobScheduler @Inject()(actorSystem: ActorSystem, forgetUserJobSqlRepositoryImpl: ForgetUserJobRepositoryImpl, kafkaProducerService: KafkaProducerService)(implicit executionContext: ExecutionContext) {
  actorSystem.scheduler.schedule(initialDelay = 0.seconds, interval = 1.minute){
    // the block of code that will be executed
    val now = new DateTime()
    println("Current time " + now)
    val resultFuture = for{
      forgetUserJobSeq  <- forgetUserJobSqlRepositoryImpl.findByCurrentDateTimeGtOrEqTargetDateTimeAndRemovedFalse(now)
      recordMetaResult <- Future.sequence(forgetUserJobSeq.map(x => kafkaProducerService.sendForgetMessage(x.ownerId)))
      _ = forgetUserJobSqlRepositoryImpl.updateRemoveFlagTrueByJobList(forgetUserJobSeq.toList)
    }yield (forgetUserJobSeq, recordMetaResult)

    resultFuture.onComplete(resultTry => resultTry match{
      case Success(tuple) => {
        println(tuple._1.size + " jobs has been update removeFlag")
        tuple._2.map(println(_))
      }
      case Failure(e) => e.printStackTrace()
    })

  }


}
