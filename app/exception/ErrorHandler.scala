package exception

import javax.inject.Singleton

import play.api.http.HttpErrorHandler
import play.api.libs.json.{JsString, Json}
import play.api.mvc.Results._
import play.api.mvc._

import scala.concurrent._

@Singleton
class ErrorHandler extends HttpErrorHandler {

  def onClientError(request: RequestHeader, statusCode: Int, message: String) = {
    Future.successful(
      Status(statusCode)(Json.obj("message" -> JsString.apply("A client error occurred: " + message)))
    )
  }

  def onServerError(request: RequestHeader, exception: Throwable) = {
    println("ServerError" + exception)
    Future.successful(
      InternalServerError(Json.obj("message" -> JsString.apply("A server error occurred: " + exception.getMessage)))
    )
  }
}