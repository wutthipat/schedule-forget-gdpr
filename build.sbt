name := """forget-log-scheduler"""
organization := "com.scale360solutions"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.4"

parallelExecution in Test := false

coverageEnabled in (test):= true

coverageExcludedPackages := ".*dto;.*controllers.javascript;.*ReverseHomeController;.*router"

libraryDependencies ++= {
  Seq(
    "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.6",
    "org.scalatest" %% "scalatest" % "3.0.1",
    "org.scalamock" %% "scalamock" % "4.0.0",
    "org.apache.kafka" %% "kafka" % "0.11.0.1"
      exclude("javax.jms", "jms")
      exclude("com.sun.jdmk", "jmxtools")
      exclude("com.sun.jmx", "jmxri")
      exclude("org.slf4j", "slf4j-simple")
      exclude("org.slf4j", "slf4j-log4j12"),
    "org.apache.kafka" % "kafka-clients" % "0.11.0.1",
    "ch.qos.logback" % "logback-classic" % "1.2.3",
    "mysql"% "mysql-connector-java"% "6.0.6",
    "com.typesafe.slick" %% "slick" % "3.2.1",
    "com.typesafe" % "config" % "1.3.1",
    "com.typesafe.slick" %% "slick-hikaricp" % "3.2.0",
    "org.apache.logging.log4j" % "log4j-api" % "2.10.0",
    "org.apache.logging.log4j" % "log4j-core" % "2.10.0",
    "com.h2database" % "h2" % "1.4.187" % "test",
    "net.manub" %% "scalatest-embedded-kafka" % "0.14.0" % "test"
  )
}
libraryDependencies += "com.enragedginger" %% "akka-quartz-scheduler" % "1.6.1-akka-2.5.x"
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.8"
libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.scale360solutions.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.scale360solutions.binders._"
