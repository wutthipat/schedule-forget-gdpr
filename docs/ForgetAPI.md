# Create conversation
This API enables shop owners to start talking with their customers.


## URL
|||
| ---------------- | :------------------------------------: |
|** Method **      |POST                                    |
|** Structure **   |`/api/v1/forget`          		    |


## Header Params

|Key                  |Value                   |Required      |Description |
| ------------------- | :--------------------: | :----------: | ---------- |
|Content-Type         |application/json        |true          |            |


## Data Params

|Field Name             |Required             |Type          |Description                     |
| --------------------- | :-----------------: | :----------: | ------------------------------ |
|ownerId                |true                 |String	     |Id of user data to be forgot    |
|timeToForgetInMinute   |true                 |Long          |The period of time in minutes that would like to forget in; 0 if immediately forget|


## Example 
Requires a JSON object with these fields for forgetting data of dpecified user id.

Example
````javascript
Scheduled forget

{
   "ownerId": "TestOwnerId",
   "timeToForgetInMinute": 3
}


Immediately forget

{
   "ownerId": "TestOwnerId",
   "timeToForgetInMinute": 0
}
````


## Success Response status 200

````javascript
HTTP status = 200
{
    "ownerId":"TestOwnerIdaaa"
}
````

## Success Response status 201

````javascript
HTTP status = 201
{
    "jobId":1
}
````

## Fail Response

### - Invalid request parameter type
````javascript
HTTP status 400
{
    "message": "The type of parameter is invalid"
}
````

### - Invalid request json format
````javascript
HTTP status 400
{
    "message": "The format of json is invalid"
}

````

### - Lost connection of MySql of Kafka Broker
````javascript
HTTP status 500
{
    "message": "exceptional message"
}
````

