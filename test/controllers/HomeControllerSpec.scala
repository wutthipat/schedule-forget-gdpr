package controllers

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.scale360solutions.dto.ForgetUserDto
import com.scale360solutions.gdprloggerhashing.model.{JobId, OwnerId}
import com.scale360solutions.gdprloggerhashing.services.ForgetUserScheduleService
import dto.ForgetUserResponseDto
import org.scalamock.scalatest.MockFactory
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.mvc.Results
import play.api.test._
import play.api.test.Helpers._
import play.api.libs.json._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}


/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 *
 * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
 */
class HomeControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting with MockFactory with Results {


  "HomeController GET" should {

    "render the index page from a new instance of controller" in {
      val forgetUserScheduleService = mock[ForgetUserScheduleService]
      val controller = new HomeController(stubControllerComponents(), forgetUserScheduleService)
      val home = controller.index().apply(FakeRequest(GET, "/"))

      status(home) mustBe OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Welcome to Play")
    }

    "render the index page from the application" in {
      val controller = inject[HomeController]
      val home = controller.index().apply(FakeRequest(GET, "/"))

      status(home) mustBe OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Welcome to Play")
    }

    "render the index page from the router" in {
      val request = FakeRequest(GET, "/")
      val home = route(app, request).get

      status(home) mustBe OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Welcome to Play")
    }

    "ForgetUserWithScheduler call service" in {
      // Mock
      implicit val actorSystem = ActorSystem("TestSystem")
      implicit val materializer = ActorMaterializer()

      val forgetUserScheduleService = mock[ForgetUserScheduleService]
      (forgetUserScheduleService.forgetUser _).expects(OwnerId("TestOwnerId"), 1).returning(Future.successful(ForgetUserResponseDto(jobId = Some(1)))).once
      val controller = new HomeController(stubControllerComponents(), forgetUserScheduleService)
      val request = FakeRequest(POST, "/api/v1/forget").withHeaders(("Content-Type", "application/json")).withJsonBody(Json.toJson(ForgetUserDto("TestOwnerId", 1)))

      // Execute
      val response = controller.forgetUserWithScheduler().apply(request)

      // Assertion
      response.onComplete{x => x match{
          case Success(a) =>  assert(a.header.status == 201)
          case Failure(e) => e.printStackTrace()
        }
      }


    }

    "ForgetUserWithScheduler bad request" in {
      // Mock
      val forgetUserScheduleService = mock[ForgetUserScheduleService]
      val controller = new HomeController(stubControllerComponents(), forgetUserScheduleService)
      val request = FakeRequest(POST, "/api/v1/forget").withHeaders(("Content-Type", "application/json")).withJsonBody(Json.toJson("TestOwnerId"))
      val response = controller.forgetUserWithScheduler().apply(request)

      response.onComplete{x => x match{
        case Success(a) =>  assert(a.header.status == 400)
        case Failure(e) => e.printStackTrace()
      }
      }
    }
  }
}
