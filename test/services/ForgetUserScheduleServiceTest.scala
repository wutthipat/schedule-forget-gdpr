package services

import com.scale360solutions.gdprloggerhashing.kafka.KafkaProducerService
import com.scale360solutions.gdprloggerhashing.model.{ForgetUserJob, JobId, OwnerId, TargetDateTime}
import com.scale360solutions.gdprloggerhashing.repositories.ForgetUserJobRepositoryImpl
import com.scale360solutions.gdprloggerhashing.services.ForgetUserScheduleService
import org.scalamock.scalatest.MockFactory
import org.scalatestplus.play.PlaySpec

import scala.concurrent.Future

class ForgetUserScheduleServiceTest extends PlaySpec with MockFactory {
  lazy val kafkaProducerService = mock[KafkaProducerService]
  lazy val forgetUserJobSqlRepositoryImpl = mock[ForgetUserJobRepositoryImpl]
  val forgetUserScheduleService = new ForgetUserScheduleService(kafkaProducerService, forgetUserJobSqlRepositoryImpl)

  "Method forgetUser " should {
    " instantly call publishMessagetoKafka once with correct ownerId when delay is zero" in {
      //Mock
      (kafkaProducerService.sendForgetMessage _).expects(OwnerId("TestOwnerId")).returning(Future.successful(null)).once

      //Execute
      forgetUserScheduleService.forgetUser(OwnerId("TestOwnerId"), 0)
    }

    "call insert from forgetUserJobSqlRepositoryImpl once with correct OwnerId and TargetDateTime" in {
      //Mock
      (forgetUserJobSqlRepositoryImpl.insert _).expects(*).returning(Future.successful(JobId(1))).once

      // Execute
      forgetUserScheduleService.forgetUser(OwnerId("TestOwnerId"), 1)
    }
  }
}

