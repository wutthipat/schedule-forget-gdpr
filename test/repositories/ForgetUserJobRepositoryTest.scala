package repositories

import com.scale360solutions.gdprloggerhashing.model._
import com.scale360solutions.gdprloggerhashing.repositories.ForgetUserJobSqlRepository
import org.joda.time.DateTime
import org.scalatest.FunSuite
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import repositories.connection.H2DBComponent

class ForgetUserJobRepositoryTest extends FunSuite with H2DBComponent with ScalaFutures with ForgetUserJobSqlRepository {
  implicit val defaultPatience = PatienceConfig(timeout = Span(5, Seconds), interval = Span(500, Millis))

  test("insert new forget_user_job should return generated job id") {
    val mockDateTime = new DateTime(2018,1,30,0,0)
    val response = insert(ForgetUserJob(ownerId = OwnerId("TestOwnerId3"), targetDateTime = TargetDateTime(mockDateTime)))
    whenReady(response){
      jobId => assert(jobId == JobId(3))
    }
  }

  test("findByCurrentDateTimeGtOrEqTargetDateTimeAndRemovedFalse must return 1 job") {
    val mockDateTime = new DateTime(2018,1,30,0,0)
    val response = findByCurrentDateTimeGtOrEqTargetDateTimeAndRemovedFalse(mockDateTime);
    whenReady(response){
      forgetUserJobList => assert(forgetUserJobList.head == ForgetUserJob(JobId(1), OwnerId("TestOwnerId1"), TargetDateTime(new DateTime(2018,1,19,0,0)), RemoveFlag(false)) && forgetUserJobList.size == 1)
    }
  }

  test("updateRemoveFlagTrueByJobList with job id 1 then findByCurrentDateTimeGtOrEqTargetDateTimeAndRemovedFalse must be empty "){
    val jobList = List(ForgetUserJob(JobId(1), OwnerId("TestOwnerId1"), TargetDateTime(new DateTime(2018,1,19,0,0))))
    updateRemoveFlagTrueByJobList(jobList)
  }


}
